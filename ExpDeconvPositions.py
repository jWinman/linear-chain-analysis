import sys
sys.path.append("libraries/")

from classImage import Image_Analysis
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco
from skimage import io
import subprocess

boundary_lss = [[87, 89, 80, 80, 80],
                [87, 89, 80, 80, 80],
                [87, 89, 80, 80, 80],
                [87, 89, 80, 80, 80]]

cut_lss = [[491, 497, 506, 505, 505],
           [838, 801, 812, 809, 814],
           [593, 792, 804, 799, 803],
           [440, 792, 804, 799, 803]]

cut_rss = [[520, 519, 529, 528, 529],
           [874, 837, 845, 843, 848],
           [620, 818, 832, 828, 830],
           [468, 818, 832, 828, 830]]

comp_parities = ["Compr0/Asymm", "Compr1/Asymm", "Compr1/Symm", "Compr2/Asymm-1", "Compr2/Asymm-2", "Compr2/Symm"]

for compr, boundary_ls, cut_ls, cut_rs in zip(comp_parities, boundary_lss, cut_lss, cut_rss):
    xs = np.zeros((len(boundary_ls), 30))
    ys = np.zeros((len(boundary_ls), 30))

    for i, (boundary_l, cut_l, cut_r) in enumerate(zip(boundary_ls, cut_ls, cut_rs)):
        IMG_name = "Images/{}/{}.png".format(compr, i)

        # load in image
        image = io.imread(IMG_name, as_gray=True)
        im_analysis = Image_Analysis(image, boundary_l=boundary_l, boundary_r=1)

        psf = im_analysis.create_psf(cut_l, cut_r)
        xs_tmp, ys_tmp = im_analysis.deconvolve_positions()
        im_analysis.PlotDeconvolution(boundary_l, "{}/Deconv{}.pdf".format(compr, i))
        #xs[i], ys[i] = xs_tmp[:30], ys_tmp[:30]


    xs_avg = np.mean(xs, axis=0)
    ys_avg = np.mean(ys, axis=0)

    xs_std = np.std(xs, axis=0)
    ys_std = np.std(ys, axis=0)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.errorbar(xs_avg, ys_avg, xerr=xs_std, yerr=ys_std, fmt="o")
    ax.axvline(abs(xs_avg[0] - xs_avg[27]) / 2.)

    ax.set_ylabel("Positions $|y|$")
    ax.set_xlabel("Positions $x$")

    fig.savefig("Images/{}/AvgPositions.pdf".format(compr))
