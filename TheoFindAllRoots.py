import sys
sys.path.append("libraries/")

from classTheoryChain import Chain
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco

# define all necessary parameters
N = 21 # has to be number of sphere + 1
parity = "Even" if (N % 2 == 1) else "Odd"
epss = np.arange(-0.02, 1.3, 0.001)
forces0 = np.linspace(0, 0.01, 1e4)[1:]
anglesLast = np.zeros(len(forces0))

# define arrays where to store symm values
forces0_symm = np.zeros(len(epss))
energies_symm = np.zeros(len(epss))
compressions_symm = np.zeros(len(epss))

# define arrays where to store calc values
all_compressions = np.array([])
all_energiesdiff = np.array([])
all_epss = np.array([])
all_forces0 = np.array([])


for i, eps in enumerate(epss):
    chain = Chain(eps, N)

    ##### Call of the root finding function ####
    roots, compressions, energies, maximums, symmetry = chain.Nl_findall_roots(forces0)
    ############################################

    # select symmetric case
#    argsymm = np.argmin(symmetry)
    args = np.where(maximums == (N - 1) // 2)[0]
    symm_compressions = compressions[args]
    argsymm = args[np.argmin(symm_compressions)]

    print("Eps: ", round(eps, 6), "Compression: ", compressions[argsymm])

    # store symm values in the arrays
    forces0_symm[i] = roots[argsymm]
    energies_symm[i] = energies[argsymm]
    compressions_symm[i] = compressions[argsymm]

    # store calc values in the arrays
    all_compressions = np.append(all_compressions, compressions)
    all_energiesdiff = np.append(all_energiesdiff, (energies - energies[argsymm]))
    all_epss = np.append(all_epss, np.ones(len(compressions)) * eps)
    all_forces0 = np.append(all_forces0, roots)

# save data for all structures
np.savetxt("data/Theory/CompressionsEnergiesDiff-{}.csv".format(N - 1), np.array([all_compressions, all_energiesdiff]).T)
np.savetxt("data/Theory/EpsForces0-{}.csv".format(N - 1), np.array([all_epss, all_forces0]).T)

# save data for symmetric structures
np.savetxt("data/Theory/CompressionsEnergiesSymm-{}.csv".format(N - 1), np.array([compressions_symm, energies_symm]).T)
np.savetxt("data/Theory/EpsForces0Symm-{}.csv".format(N - 1), np.array([epss, forces0_symm]).T)
