This code repository contains the code for the nonlinear stepwise method as well as image analysis for the experimental part.
All code related to the stepwise method contains the prefix "Theo" in the file name, while experimental analysis code has the prefix "Exp".

There are the following directories:
- libraries:
    - classImage.py: contains image analysis code for experiments
    - classTheoryChain.py: contains code for stepwise method

PlotScripts:
    - Plot scripts for various purposes

Plots:
    - Plots and pdfs

Images:
    - Experimental raw images for image analysis

data:
    - csv or txt files for plots
    - I save and read from here to keep all data files together
