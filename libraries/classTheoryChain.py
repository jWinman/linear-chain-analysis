import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco
import scipy.spatial.distance as ssd

class Chain:
    def __init__(selfy, eps, N):
        """
        Konstructor to define all basic variables

        Parameters
        ----------
        eps: epsilon related to G0
        N: number of spheres + 1 (phantom sphere inside the wall)
        """

        selfy.N, selfy.eps = N, eps
        selfy.G0inv = 4 + selfy.eps

        selfy.forces = np.zeros(N) # 1darray for forces = 0
        selfy.angles = np.zeros(N) # 1darray for angles = 0

        selfy.xs = np.zeros(N) # 1darray for positions x = 0
        selfy.ys = np.zeros(N) # 1darray for positions y = 0

        selfy.inflectionPoints = np.array([]) # 1darray for inflection points, but not used so far

    def Nl_force_update(selfy, force, angle):
        """
        Calculate force from previous force and angle

        Parameters
        ----------
        force: force of previous sphere
        angle: angle of previous sphere

        Returns
        -------
        force of current sphere
        """
        return np.sin(np.arctan(selfy.G0inv * force - np.tan(angle))) - force

    def Nl_angle_update(selfy, force, angle):
        """
        Calculate angle from previous force and angle

        Parameters
        ----------
        force: force of previous sphere
        angle: angle of previous sphere

        Returns
        -------
        angle of current sphere
        """
        return np.arctan(selfy.G0inv * force - np.tan(angle))

    def Nl_iterative(selfy, inits):
        """
        Iterates over all spheres + 1 and
        stores angles in selfy.angles and
        forces in selfy.forces

        Parameters
        ----------
        inits: force for the first sphere

        Returns
        -------
        angle of phantom sphere inside the wall
        """

        selfy.forces[0] = inits
        selfy.angles[0] = 0
        for i in range(1, selfy.N):
            selfy.angles[i] = selfy.Nl_angle_update(selfy.forces[i - 1], selfy.angles[i - 1])
            selfy.forces[i] = selfy.Nl_force_update(selfy.forces[i - 1], selfy.angles[i - 1])

        return selfy.angles[-1]

    def Nl_find_root(selfy, bracket):
        """
        Finds root of Nl_iterative(selfy, inits)
        between given bracket

        Parameters
        ----------
        bracket: 1darray of length 2; defines bracket in-between root should be found

        Returns
        -------
        root of the function
        """

        sol = sco.root_scalar(selfy.Nl_iterative, method="bisect", bracket=bracket)
        print("Solution: ", sol.root, selfy.Nl_iterative(sol.root))
        return sol.root

    def Nl_findall_roots(selfy, lin_forces):
        """
        Finds ALL roots of Nl_iterative(selfy, inits)
        on the grid/lattice defined by lin_forces at a given epsilon

        Parameters
        ----------
        lin_forces: 1darray defining a lattice where to search for roots

        Returns
        -------
        roots: all the roots found
        compressions: compressions for all the roots
        energies: energies for all the roots
        maximum: argument of the maximum angle
        """

        roots = np.array([])
        compressions = np.array([])
        energies = np.array([])
        maximum = np.array([])
        symmetry = np.array([])

        final_angles = np.zeros(2)
        final_angles[0] = selfy.Nl_iterative(lin_forces[0])
        for i in range(1, len(lin_forces) - 1):
            final_angles[i % 2] = selfy.Nl_iterative(lin_forces[i])
            if (np.sign(final_angles[0]) != np.sign(final_angles[1])):
                bracket = lin_forces[i - 1:i + 1]
                sol = sco.root_scalar(selfy.Nl_iterative, method="bisect", bracket=bracket)
                roots = np.append(roots, sol.root)
                compressions = np.append(compressions, selfy.Compression())
                energies = np.append(energies, selfy.getEnergy())
                maximum = np.append(maximum, selfy.getMaxArg())
                symmetry = np.append(symmetry, selfy.getSymmetric())

        return roots, compressions, energies, maximum, symmetry

    def Compression(selfy):
        """
        Calculates the compression selfy.compr = N - L/d of the system
        Stores it in selfy.compr

        Returns
        -------
        Compression
        """

        selfy.compr = selfy.N - np.sum(np.cos(selfy.angles))
        return selfy.compr

    def CalcPos(selfy):
        """
        Calculates positions x of y of the system
        Stores values in selfy.xs and selfy.ys
        """

        selfy.ys = np.copy(selfy.forces)
        selfy.ys[1::2] *= -1

        selfy.xs[0] = 0.5 * np.cos(selfy.angles[0])
        for i in range(1, selfy.N):
            selfy.xs[i] = selfy.xs[i - 1] + np.cos(selfy.angles[i])

    def getSymmetric(selfy):
        if (selfy.N % 2 == 0):
            return sum((selfy.forces[:(selfy.N - 1) // 2] - selfy.forces[(selfy.N - 1) // 2 + 1:-1])**2)
        else:
            return sum((selfy.angles[:(selfy.N - 1) // 2] - selfy.angles[(selfy.N - 1) // 2 + 1:][::-1])**2)

    def getMaxAngle(selfy):
        """
        Finds the maximum angle between the spheres

        Returns
        -------
        maximum of the angles
        """

        return max(selfy.angles)

    def getMaxPosition(selfy):
        """
        Find the x-position of the maximum force/y-position

        Returns
        -------
        maximum of the argument of forces
        """

        return selfy.xs[np.argmax(selfy.forces)]

    def getMaxArg(selfy):
        """
        Find the argument/index of the maximum angle between all spheres

        Returns
        -------
        argument of maximum of angles
        """

        return np.argmax(selfy.angles) if (selfy.N % 2 == 1) else np.argmax(selfy.forces)

    def getEnergy(selfy):
        """
        Calculate the (rotational) energy of the system

        Returns
        -------
        Rotational energy
        """

        return 0.5 * np.sum(selfy.forces[:-1]**2)

    def plotAnglesForces(selfy, path):
        """
        Plots Angles vs Forces under path
        """

        fig =plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        ax.set_title(r"$\epsilon = {:.3f}$ and $N - L / d = {:.3f}$".format(selfy.eps, selfy.compr))

        ax.plot(selfy.ys, "o", label="Forces")
        ax.plot(selfy.angles, "o", label="Angles")
        ax.plot([0, selfy.N], [0, 0], "-")

        ax.set_xlabel("Sphere number $n_i$")
        ax.legend()

        fig.savefig("Plots/Theory/{}".format(path))

    def plotPos(selfy, path):
        """
        Plots x-positions vs y-positions under path
        """

        quadratic = lambda x, a, b, c: a * (x - b)**2 + c
        ys_abs = abs(selfy.ys)
        (a, b, c), _ = sco.curve_fit(quadratic, selfy.xs[np.argmax(ys_abs) - 1:np.argmax(ys_abs) + 2],
                                                ys_abs[np.argmax(ys_abs) - 1:np.argmax(ys_abs) + 2])

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1, aspect="equal")
        energy = selfy.getEnergy()
        compr = selfy.Compression()

        ax.plot(selfy.xs[:-1], selfy.ys[:-1], "ro", markersize=3.)
        ax.axvline((selfy.xs[-2] + 0.5) / 2., color="k")
        ax.axvline(b, linestyle="--", color="r")

        for x, y in zip(selfy.xs[:-1], selfy.ys[:-1]):
            c = plt.Circle((x, y), 0.5, color="b", fill=False)
            ax.add_patch(c)

        ax.axhline(0)
        ax.set_ylim(-1, 1)
        ax.set_xlim(0, selfy.N - 1)

        ax.set_yticks([])

        fig.savefig("Plots/Theory/{}".format(path))

        return selfy.xs[:-1], abs(selfy.ys[:-1])

    def fitAngles(selfy, path, fit_n):
        """
        Fit a A * sinh(B * x + C) to the first fit_n (x-positions, y-positions)

        Returns
        -------
        A, B, C: Fit parameters of sinh
        """

        sinh_fit = lambda x, A, B, C: A * np.sinh(B * x + C)
        x_sinh = np.linspace(1, selfy.N / 2, 100)

        A, B, C = 4 * selfy.forces[0] / np.sqrt(selfy.eps), np.sqrt(selfy.eps), 0

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.set_title("Compression $\Delta = {:.3f}$".format(selfy.compr))

        ax.plot(np.arange(1, 22, 1), selfy.angles, "bo")
#        ax.plot(x_sinh, sinh_fit(x_sinh - 1, A, B, C), "-")

        ax.set_xlabel(r"Sphere number $n$")
        ax.set_ylabel(r"Angle $\theta_n$")
        ax.set_xlim(0, 21)
        ax.set_xticks(np.arange(0, 21, 2))
        ax.axvline(11, linestyle="--", linewidth=2.)
        ax.grid()

        fig.savefig("Plots/Theory/Fits/{}".format(path))

        return A, B

    def fitForces(selfy, path, fit_n):
        """
        Fit a A * sinh(B * x + C) to the first fit_n (x-positions, y-positions)

        Returns
        -------
        A, B, C: Fit parameters of sinh
        """

        fit = lambda x, A, B, C: A * np.sinh(B * x) + C * np.cosh(B * x)
        x_sinh = np.linspace(0, selfy.N / 2 - 1, 100)

        A, B, C = 2 * selfy.forces[0] / np.sqrt(selfy.eps), np.sqrt(selfy.eps), selfy.forces[0]

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.set_title("Compression $\Delta = {:.3f}$".format(selfy.compr))

        ax.plot(selfy.forces, "bo")
        ax.plot(x_sinh, fit(x_sinh, A, B, C), "-")

        ax.set_xlabel(r"Sphere number $n$")
        ax.set_ylabel(r"Forces $F_n$")
        ax.set_xlim(0, 20)
        ax.set_xticks(np.arange(0, 20, 2))
        ax.axvline(9.5, linestyle="--", linewidth=2.)
        ax.grid()

        fig.savefig("Plots/Theory/Fits/{}".format(path))

        return A, B, C

    def plotF0Angles(selfy, path):
        forces = lambda x, A, B, C: A * np.sinh(B * x) + C * np.cosh(B * x)
        x_sinh = np.linspace(0, 8, 100)

        A_forces, B_forces, C_forces = 2 * selfy.forces[0] / np.sqrt(selfy.eps), np.sqrt(selfy.eps), selfy.forces[0]

        angles = lambda x, A, B, C: A * np.sinh(B * x + C)
        A_angles, B_angles, C_angles = 4 * selfy.forces[0] / np.sqrt(selfy.eps), np.sqrt(selfy.eps), 0

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.set_title("Compression $\Delta L = {:.3f}$".format(selfy.compr))

        ax.plot(selfy.angles, selfy.forces, "bo")
        ax.plot(angles(x_sinh, A_angles, B_angles, C_angles), forces(x_sinh, A_forces, B_forces, C_forces), "-")

        ax.set_xlabel(r"Angles $\theta_n$")
        ax.set_ylabel(r"Forces $F_n$")
        ax.grid()

        fig.savefig("Plots/Theory/F0Angles/{}".format(path))


    def printPos(selfy, path):
        """
        Save x- and y-positions in csv file under path
        """

        np.savetxt(path, np.array([selfy.xs, selfy.ys]).T)

    def printAngles(selfy, path):
        """
        Save angles and forces in csv file under path
        """

        np.savetxt(path, np.array([np.arange(selfy.N), selfy.angles, selfy.forces]).T)
