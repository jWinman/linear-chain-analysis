import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco
import uncertainties as un
from skimage import feature, filters, morphology

class Image_Analysis:
    def __init__(selfy, image, boundary_l=30, boundary_r=1):
        selfy.image_raw = image
        selfy.image = image[:, boundary_l:-boundary_r]
        selfy.image_psf = np.copy(selfy.image)
        selfy.image_deconvolved = np.copy(selfy.image)

        selfy.threshold = filters.threshold_otsu(selfy.image)
        selfy.image_bin = np.where(selfy.image > selfy.threshold, selfy.image, 0)
        selfy.image_bin_eros = selfy.image > selfy.threshold

        selfy.xs, selfy.ys, selfy.ys_rel = None, None, None
        selfy.thetas = None

        selfy.a, selfy.b = 0, 0
        selfy.t1, selfy.t2s = np.zeros(2), None
        selfy.d, selfy.L = None, None

        selfy.linear_fit = lambda x, a, b: a * x + b

    def calc_sphere_positions(selfy, erosion, min_sigma=5, max_sigma=16, num_sigma=10, threshold=0.03, manual=1.0):
        # binarise erode image
        for _ in range(erosion):
            selfy.image_bin_eros = morphology.binary_erosion(selfy.image_bin_eros)

        # blob detection to get centre points of spheres
        ys, xs, _ = feature.blob_doh(selfy.image_bin_eros, min_sigma=min_sigma,
                                                           max_sigma=max_sigma,
                                                           num_sigma=num_sigma,
                                                           threshold=threshold).T
        selfy.xs, selfy.ys = np.sort(xs), ys[np.argsort(xs)]
        selfy.xs, selfy.ys = selfy.xs[1:-1][::-1], selfy.ys[1:-1][::-1]

        # fit line to all spheres
        (selfy.a, selfy.b), _ = sco.curve_fit(selfy.linear_fit, selfy.xs, selfy.ys)
        selfy.ys_rel = selfy.ys - selfy.linear_fit(selfy.xs, selfy.a, selfy.b)

        # Calc diameter
        selfy.d = np.mean(np.sqrt(np.diff(selfy.xs[:5])**2 + np.diff(selfy.ys[:5])**2))
        selfy.L = np.sqrt((selfy.xs[1] - selfy.xs[-1])**2 + (selfy.ys[1] - selfy.ys[-1])**2)

        return selfy.xs - selfy.xs[-1] - selfy.L / 2., abs(selfy.ys_rel), selfy.d

    def calc_angles(selfy):
        # calc vectors between between line sphere1--sphere2
        selfy.t1 = -1. / np.sqrt(1 + selfy.a**2) * np.array([1, selfy.a])
        selfy.t2s = np.array([np.diff(selfy.xs), np.diff(selfy.ys)]).T
        norms = np.linalg.norm(selfy.t2s, axis=1)

        thetas = np.zeros(len(norms))

        for i, (t2, norm) in enumerate(zip(selfy.t2s, norms)):
            thetas[i] = np.arccos(np.dot(selfy.t1, t2) / norm)

        selfy.thetas = thetas

        return thetas

    def create_psf(selfy, left, right):
        image_sphere = selfy.image_bin[:, left:right].T

        cut_out = right - left
        total_length = len(selfy.image_bin.T)
        add_pixel = (total_length - cut_out) % 2

        zeros_left = np.zeros((np.shape(selfy.image)[0], (total_length - cut_out) // 2 + add_pixel)).T
        zeros_right = np.zeros((np.shape(selfy.image)[0], (total_length - cut_out) // 2)).T

        selfy.image_psf = np.concatenate((zeros_left, image_sphere, zeros_right)).T

        return selfy.image_psf

    def deconvolve_positions(selfy, min_sigma=1, max_sigma=2, num_sigma=5, threshold=0.1):
        from scipy import fftpack
        from scipy.signal import wiener

        selfy.image_bin = wiener(selfy.image_bin)

        image_fft = fftpack.fftshift(fftpack.fftn(selfy.image_bin))
        psf_fft = fftpack.fftshift(fftpack.fftn(selfy.image_psf))
        selfy.image_deconvolved = fftpack.fftshift(fftpack.ifftn(fftpack.ifftshift(image_fft/psf_fft))).real
#        threshold_deconv = filters.threshold_otsu(image_deconvolved) / 10
#        selfy.image_deconvolved = np.where(image_deconvolved < threshold_deconv, 0, image_deconvolved)

        ys, xs, _ = feature.blob_log(selfy.image_deconvolved, min_sigma=min_sigma,
                                                              max_sigma=max_sigma,
                                                              num_sigma=num_sigma,
                                                              threshold=threshold).T

        selfy.xs, selfy.ys = np.sort(xs), ys[np.argsort(xs)]

        # fit line to all spheres
        (selfy.a, selfy.b), _ = sco.curve_fit(selfy.linear_fit, selfy.xs, selfy.ys)
        selfy.ys_rel = selfy.ys - selfy.linear_fit(selfy.xs, selfy.a, selfy.b)

        return selfy.xs, abs(selfy.ys_rel)

    def plot(selfy, name, boundary_l, boundary_r):
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        ax.imshow(selfy.image_raw, cmap=plt.cm.gray, origin="lower left")
        ax.plot(selfy.xs + boundary_l, selfy.ys, "g.")
#        ax.plot(selfy.xs[1] + boundary_l, selfy.ys[1], "ro")
#        ax.plot(selfy.xs[-1] + boundary_l, selfy.ys[-1], "ro")
        ax.axvline(selfy.L / 2. + selfy.xs[-1] + boundary_l)

        fit_xs = np.linspace(0, len(selfy.image_raw.T), 100)
        ax.plot(fit_xs, selfy.linear_fit(fit_xs, selfy.a, selfy.b), linewidth=1.)

        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xlim(0, np.shape(selfy.image_raw)[1])
        ax.set_ylim(0, np.shape(selfy.image_raw)[0])

        fig.savefig("Images/{}".format(name))

    def PlotDeconvolution(selfy, boundary_l, path):
        fig = plt.figure()
        ax = fig.add_subplot(4, 1, 1)
        ax.imshow(selfy.image_psf, cmap=plt.cm.gray, origin="lower left")
        ax.set_xlim(-abs(len(selfy.image_psf.T) - len(selfy.image_raw.T)), len(selfy.image_psf.T))

        ax = fig.add_subplot(4, 1, 2)
        ax.imshow(selfy.image_deconvolved, cmap=plt.cm.gray, origin="lower left")
        ax.plot(selfy.xs, selfy.ys, "wo")
        ax.set_xlim(-abs(len(selfy.image_psf.T) - len(selfy.image_raw.T)), len(selfy.image_psf.T))

        ax = fig.add_subplot(4, 1, 3)
        ax.imshow(selfy.image_bin, cmap=plt.cm.gray, origin="lower left")
        ax.plot(selfy.xs, selfy.ys, "go")
#        ax.axvline(abs(selfy.xs[0] - selfy.xs[27]) / 2.)
        ax.set_xlim(-abs(len(selfy.image_psf.T) - len(selfy.image.T)), len(selfy.image_psf.T))

        ax = fig.add_subplot(4, 1, 4)
        ax.imshow(selfy.image_raw, cmap=plt.cm.gray, origin="lower left")
        ax.plot(selfy.xs + boundary_l, selfy.ys, "go")
#        ax.axvline(abs(selfy.xs[0] - selfy.xs[27]) / 2. + boundary_l)

        fig.savefig("Images/" + path)

    def PlotErosion(selfy, boundary_l, boundary_r, path):
        fig = plt.figure()
        ax = fig.add_subplot(2, 1, 1)
        ax.imshow(selfy.image_bin_eros, cmap=plt.cm.gray, origin="lower left")
        ax.plot(selfy.xs, selfy.ys, "go")
        ax.plot(selfy.xs[1], selfy.ys[1], "ro")
        ax.plot(selfy.xs[-1], selfy.ys[-1], "ro")
        ax.set_xlim(-boundary_l, len(selfy.image_bin_eros.T) + boundary_r)
        ax.axvline(selfy.L / 2. + selfy.xs[-1])

        ax = fig.add_subplot(2, 1, 2)
        ax.imshow(selfy.image_raw, cmap=plt.cm.gray, origin="lower left")
        ax.plot(selfy.xs + boundary_l, selfy.ys, "go")
        ax.plot(selfy.xs[1] + boundary_l, selfy.ys[1], "ro")
        ax.plot(selfy.xs[-1] + boundary_l, selfy.ys[-1], "ro")
        ax.axvline(selfy.L / 2. + selfy.xs[-1] + boundary_l)

        fig.savefig("Images/" + path)

