import sys
sys.path.append("libraries/")

from classTheoryChain import Chain

import numpy as np
import matplotlib.pyplot as plt

N, compr = 20, 0.60
compressions, energies = np.loadtxt("data/Theory/CompressionsEnergiesDiff-{}.csv".format(N - 1), unpack=True)
epss, forces0 = np.loadtxt("data/Theory/EpsForces0-{}.csv".format(N - 1), unpack=True)

epss = epss[np.round(compressions, 2) == compr]
forces0 = forces0[np.round(compressions, 2) == compr]
energies = energies[np.round(compressions, 2) == compr]
compressions = compressions[np.round(compressions, 2) == compr]


for eps, force0 in zip(epss, forces0):
    if (round(eps, 3) == 0.422):
        chain = Chain(eps, N)
        print(eps)

        final_angle = chain.Nl_iterative(force0)
        chain.CalcPos()
        E = chain.getEnergy()

#        chain.printPos("data/Theory/Bifurcation/PosE{:.6f}-{}.csv".format(eps, N))
        chain.plotPos("HigherEnergies/PosE{:.6f}-{}.pdf".format(E, N))
