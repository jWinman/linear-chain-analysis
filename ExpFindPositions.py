import sys
sys.path.append("libraries/")

from classImage import Image_Analysis
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco
from skimage import io
import subprocess

N = 34
paths, Ls_total = np.loadtxt("Images/Length.csv", unpack=True, dtype=str)
Ls_total = np.array(Ls_total, dtype=float)
StopperDepth = np.loadtxt("Images/StopperMeasures.csv")

Ls = Ls_total - np.sum(StopperDepth)
compressions = N - Ls / 3.03

boundary_ls = np.array([50, 50, 50, 50, 50, 50, 40, 50, 50])
boundary_rs = np.array([10, 15, 10, 10, 10, 10, 10, 20, 10])
erosions = np.array([6, 9, 8, 7, 9, 19, 11, 14, 10])
max_thetas = np.zeros(len(erosions))
max_thetas_std = np.zeros(len(erosions))

for i, (path, L, erosion, compression, boundary_l, boundary_r) in enumerate(zip(paths, Ls, erosions, compressions, boundary_ls, boundary_rs)):
    print("Path: ", path)
    revolutions = 3 if (path == "Compr2/Symm") else 5
    no_spheres = 29 if (path == "Compr2/Symm") else 30
    xs = np.zeros((revolutions, no_spheres))
    ys = np.zeros((revolutions, no_spheres))
    thetas = np.zeros((revolutions, no_spheres - 1))

    for j in range(revolutions):
        IMG_name = "Images/{}/{}.png".format(path, j)

        # load in image
        image = io.imread(IMG_name, as_gray=True)
        im_analysis = Image_Analysis(image, boundary_l=boundary_l, boundary_r=boundary_r)
        if (path == "Compr2/Symm"):
            xs_tmp, ys_tmp, d = im_analysis.calc_sphere_positions(erosion, threshold=0.035)
        elif (path == "Compr3/Asymm"):
            xs_tmp, ys_tmp, d = im_analysis.calc_sphere_positions(erosion, threshold=0.025)
        elif (path == "Compr4/Symm"):
            xs_tmp, ys_tmp, d = im_analysis.calc_sphere_positions(erosion, min_sigma=20, max_sigma=50, threshold=0.04)
        else:
            xs_tmp, ys_tmp, d = im_analysis.calc_sphere_positions(erosion)

        thetas[j] = im_analysis.calc_angles()[:no_spheres - 1]

        im_analysis.PlotErosion(boundary_l, boundary_r, "{}/Eros{}.png".format(path, j))
        im_analysis.plot("{}/Modified{}.png".format(path, j), boundary_l, boundary_r)
        xs[j], ys[j] = xs_tmp[:no_spheres] / d, ys_tmp[:no_spheres] / d

    xs_avg = np.mean(xs, axis=0)
    ys_avg = np.mean(ys, axis=0)
    thetas_avg = np.mean(thetas, axis=0)

    xs_std = np.std(xs, axis=0)
    ys_std = np.std(ys, axis=0)
    thetas_std = np.std(thetas, axis=0)

    max_thetas[i] = max(thetas_avg)
    max_thetas_std[i] = thetas_std[np.argmax(thetas_avg)]

    fig = plt.figure()
    ax = fig.add_subplot(2, 1, 1)

    ax.errorbar(xs_avg, ys_avg, xerr=xs_std, yerr=ys_std, fmt="o")
    ax.axvline(0)

    ax.set_ylabel("Positions $|y|$")
    ax.set_xlabel("Positions $x$")

    ax = fig.add_subplot(2, 1, 2)

    ax.errorbar(np.arange(no_spheres - 1), thetas_avg, yerr=thetas_std, fmt="o")

    ax.set_ylabel(r"Angle $\theta$")
    ax.set_xlabel("sphere number $n$")

    fig.savefig("Images/{}/AvgPositions.pdf".format(path))

np.savetxt("data/MaxAnglesExpShift-{}.csv".format(N), np.array([compressions, max_thetas, max_thetas_std]).T)
