import sys
sys.path.append("libraries/")

from classTheoryChain import Chain
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco

N = 20
epss, forces0 = np.loadtxt("data/Theory/EpsForces0Symm-{}.csv".format(N), unpack=True)
print(epss)

maxAngles = np.zeros(len(epss))
compressions = np.zeros(len(epss))
AsTheta, BsTheta = np.zeros((len(epss), 2)).T
AsF0, BsF0, CsF0 = np.zeros((len(epss), 3)).T

roots = np.zeros(len(epss))

for i, (eps, force0) in enumerate(zip(epss, forces0)):
    chain = Chain(eps, N + 1)
    chain.Nl_iterative(force0)
    chain.CalcPos()

    compressions[i] = chain.Compression()
    maxAngles[i] = chain.getMaxAngle()
#    print("Compression: ", compressions[i], "eps: ", eps)

    if (round(compressions[i], 3) == 0.503 or i == 0):
        chain.printPos("data/Theory/PosEps{:.3f}.csv".format(eps))

    if (eps < 0.1):
        AsTheta[i], BsTheta[i] = chain.fitAngles("FitThetas{:.3f}.pdf".format(eps), 6)
        AsF0[i], BsF0[i], CsF0[i] = chain.fitForces("FitF0{:.3f}.pdf".format(eps), 6)
        chain.plotF0Angles("F0Angles{:.3f}.pdf".format(eps))


np.savetxt("data/Theory/FitParams-{}.csv".format(N), np.array([epss, forces0, compressions, AsF0, BsF0, CsF0]).T)
np.savetxt("data/MaxAnglesTheory-{}.csv".format(N), np.array([compressions, maxAngles]).T)
np.savetxt("data/Theory/EpsCompressionSymmetric-{}.csv".format(N), np.array([compressions, epss, forces0]).T)
