import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco

xAdil, yAdil = np.loadtxt("../data/AdilData/19_497.csv", unpack=True)
xTheo_low, yTheo_low = np.loadtxt("../data/Theory/PosEps0.001.csv", unpack=True)
#xTheo1, yTheo1 = np.loadtxt("../data/Theory/PosEps0.279.csv", unpack=True)
xTheoF, yTheoF = np.loadtxt("../data/Theory/Bifurcation/PosE0.157288.csv", unpack=True)
xTheoSymm, yTheoSymm = np.loadtxt("../data/Theory/Bifurcation/PosE0.157389.csv", unpack=True)

yTheoF = abs(yTheoF)
yTheoSymm = abs(yTheoSymm)
yTheo_low = abs(yTheo_low)

quadratic = lambda x, a, b, c: a * (x - b)**2 + c
(a, b, c), _ = sco.curve_fit(quadratic, xTheoF[np.argmax(yTheoF) - 1:np.argmax(yTheoF) + 2], yTheoF[np.argmax(yTheoF) - 1:np.argmax(yTheoF) + 2])
centreS1 = (xTheo_low[-1] - 0.5) / 2.
centreS2 = (xTheoF[-1] - 0.5) / 2.

#fig = plt.figure(figsize=(5.5, 4.4))
fig = plt.figure(figsize=(5.0, 2.8))
ax = fig.add_subplot(1, 1, 1)

#ax.plot(xTheo1[:-1], abs(yTheo1)[:-1], "o", label=" at $\Delta L = 0.50$")
#ax.plot(xAdil, yAdil, "r+", markersize=10, label="Energy minimisation at $\Delta L = 0.50$")
ax.plot(xTheo_low[:-1], yTheo_low[:-1], "g^:", linewidth=0.7, markersize=5.,label="Symmetric structure S \n at $\Delta = 0.08$")
ax.plot(xTheoSymm[:-1], yTheoSymm[:-1], "b*:", linewidth=0.7, markersize=5., label="Symmetric structure S \n at $\Delta = 0.65$")
ax.plot(xTheoF[:-1], yTheoF[:-1], "r+:", linewidth=0.7, markersize=5., label="Asymmetric structure F \n at $\Delta = 0.65$")

print(xTheo_low[-2])

ax.axvline(b, linestyle=":", linewidth=1., color="r")
ax.axvline(centreS1, linestyle="-", linewidth=1., color="g")
ax.axvline(centreS2, linestyle="--", linewidth=1., color="b")

ax.set_ylabel("Displacement $r_n$")
ax.set_xlabel("Position $x_n$")
ax.set_xlim(0, 20)
ax.set_ylim(top=0.35)
#ax.legend(loc="upper right", fontsize=10)

fig.savefig("../Plots/AdilComparison.pdf")
