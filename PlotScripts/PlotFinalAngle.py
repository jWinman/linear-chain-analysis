import sys
sys.path.append("../libraries/")

from classTheoryChain import Chain
import numpy as np
import matplotlib.pyplot as plt

N = 21
epss = np.arange(0.01, 1.0, 0.05)

for eps in epss:
    forces0 = np.linspace(0., 0.02, 1e4)[1:]
    anglesLast = np.zeros(len(forces0))
    energies = np.zeros(len(forces0))
    compressions = np.zeros(len(forces0))

    for i, force0 in enumerate(forces0):
        chain = Chain(eps, N)
        anglesLast[i] = chain.Nl_iterative(force0)
        energies[i] = chain.getEnergy()
        compressions[i] = chain.Compression()

    fig = plt.figure()
    ax = fig.add_subplot(3, 1, 1)
    ax.set_title(r"$\epsilon = {}$".format(eps))

    ax.plot(forces0, anglesLast, "-")
    ax.axhline(0, color="k")
    ax.grid()
    ax.set_ylabel(r"Final angle $\theta_N$")
    ax.set_xlabel(r"Initial force $F_0$")

    ax = fig.add_subplot(3, 1, 2)

    ax.plot(forces0, energies, "-")
    ax.grid()
    ax.set_ylabel(r"Energy $E$")
    ax.set_xlabel(r"Initial force $F_0$")

    ax = fig.add_subplot(3, 1, 3)

    ax.plot(forces0, compressions, "-")
    ax.set_ylim(top=1.3)
    ax.grid()
    ax.set_ylabel(r"$N - L / d$")
    ax.set_xlabel(r"Initial force $F_0$")

    fig.savefig("../Plots/Theory/FinalAngle/ForcesVsAngles{:.3f}.pdf".format(eps))

