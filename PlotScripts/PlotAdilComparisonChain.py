import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco

def PlotChain(xs, ys, path):
    N = len(xs)
    quadratic = lambda x, a, b, c: a * (x - b)**2 + c
    ys_abs = abs(ys)
    (a, b, c), _ = sco.curve_fit(quadratic, xs[np.argmax(ys_abs) - 1:np.argmax(ys_abs) + 2],
                                            ys_abs[np.argmax(ys_abs) - 1:np.argmax(ys_abs) + 2])

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, aspect="equal")

    ax.plot(xs[:-1], ys[:-1], "ro", markersize=3.)
    ax.axvline((xs[-2] + 0.5) / 2., color="k")
    ax.axvline(b, linestyle="--", color="r")

    for x, y in zip(xs[:-1], ys[:-1]):
        c = plt.Circle((x, y), 0.5, color="b", fill=False)
        ax.add_patch(c)

    ax.axhline(0)
    ax.set_ylim(-1, 1)
    ax.set_xlim(0, N - 1)

    ax.set_yticks([])

    fig.savefig("../Plots/{}".format(path))

xTheo_low, yTheo_low = np.loadtxt("../data/Theory/PosEps0.001.csv", unpack=True)
#xTheo1, yTheo1 = np.loadtxt("../data/Theory/PosEps0.279.csv", unpack=True)
xTheoF, yTheoF = np.loadtxt("../data/Theory/Bifurcation/PosE0.157288.csv", unpack=True)
xTheoSymm, yTheoSymm = np.loadtxt("../data/Theory/Bifurcation/PosE0.157389.csv", unpack=True)

PlotChain(xTheo_low, yTheo_low, "Theo_low.pdf")
PlotChain(xTheoF, yTheoF, "Theo_F.pdf")
PlotChain(xTheoSymm, yTheoSymm, "Theo_symm.pdf")

