import numpy as np
import matplotlib.pyplot as plt

def analytic(thetas):
    return 3 - np.cos(thetas) - 2 * np.cos(np.arcsin(0.5 * np.sin(thetas)))

NSim = 20
Ntheo = 20
NExp = 34
L_sim, cosA = np.loadtxt("../data/MaxAnglesAdil.csv", unpack=True)
compressions_theo, thetas_theo = np.loadtxt("../data/MaxAnglesTheory-{}.csv".format(Ntheo), unpack=True)
compressions_exp, thetas_exp, thetas_std_exp = np.loadtxt("../data/MaxAnglesExp-{}.csv".format(NExp), unpack=True)
compressions_exp_shift, thetas_exp_shift, thetas_std_exp_shift = np.loadtxt("../data/MaxAnglesExpShift-{}.csv".format(NExp), unpack=True)

compressions_sim = NSim - L_sim # Nd - L
thetas_sim = np.arccos(cosA)

thetas_theo = thetas_theo[compressions_theo < 0.9]
compressions_theo = compressions_theo[compressions_theo < 0.9]

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(compressions_sim, thetas_sim, "o", label="Energy minimisation", clip_on=False)
ax.plot(compressions_theo, thetas_theo, "-", label="Stepwise method", linewidth=2.)
ax.errorbar(np.array([compressions_exp[2], compressions_exp[5], compressions_exp[7], compressions_exp[-1]]),
            np.array([thetas_exp[2], thetas_exp[5], thetas_exp[7], thetas_exp[-1]]),
            xerr=0.02,
            yerr=np.array([thetas_std_exp[2], thetas_std_exp[5], thetas_std_exp[7], thetas_std_exp[-1]]),
            fmt="gx", zorder=300, alpha=0.3, label="Raw experimental data")

ax.errorbar(np.array([compressions_exp_shift[2], compressions_exp_shift[5], compressions_exp_shift[7], compressions_exp_shift[-1]]),
            np.array([thetas_exp_shift[2], thetas_exp_shift[5], thetas_exp_shift[7], thetas_exp_shift[-1]]),
            xerr=0.02,
            yerr=np.array([thetas_std_exp_shift[2], thetas_std_exp_shift[5], thetas_std_exp_shift[7], thetas_std_exp_shift[-1]]),
            fmt="gx", label="Adjusted experimental data", zorder=300)


ax.set_ylabel(r"Maximum angle $\theta_{\text{max}}$")
ax.set_xlabel(r"Compression $\Delta$")
ax.set_ylim(bottom=0, top=2.0)
ax.set_xlim(left=0)
ax.legend()

fig.savefig("../Plots/MaxThetas.pdf")

