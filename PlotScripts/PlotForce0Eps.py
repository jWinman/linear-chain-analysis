import numpy as np
import matplotlib.pyplot as plt

N = 20
epss, forces0 = np.loadtxt("../data/Theory/EpsForces0-{}.csv".format(N), unpack=True)
epssSymm, forces0Symm = np.loadtxt("../data/Theory/EpsForces0Symm-{}.csv".format(N), unpack=True)
G0Symm = 1 / (4 + epssSymm)

fig = plt.figure(figsize=(10, 6.5))
ax = fig.add_subplot(1, 1, 1)

ax.plot(1 / (4 + epss), forces0, ".", markersize=1)
ax.plot(G0Symm[G0Symm > 0.2085], forces0Symm[G0Symm > 0.2085], "r.", markersize=1)

#ax.set_title("$N = {}$".format(N), fontsize=24)

ax.set_ylabel("Force $F_1$", fontsize=24)
ax.set_xlabel("Compressive force $G_0$", fontsize=24)
ax.set_xlim(0.19, 0.25)
ax.set_ylim(0., 0.01)

#ax.text(0.251, 0.0098, "(a)" if (N == 20) else "(b)", fontsize=24)
ax.text(0.247, 0.0053 if (N == 20) else 0.0058, "S", color="red", fontsize=24)


fig.savefig("../Plots/Theory/ChaosPlots/EpsForces0-{}.png".format(N), dpi=500)
