import numpy as np
import matplotlib.pyplot as plt

N = 20
compressions, energies = np.loadtxt("../data/Theory/CompressionsEnergiesDiff-{}.csv".format(N), unpack=True)
compressionsSymm, energiesSymm = np.loadtxt("../data/Theory/CompressionsEnergiesSymm-{}.csv".format(N), unpack=True)

compressions0 = compressions[energies == 0]
energies0 = energies[energies == 0]
compressions = compressions[energies != 0]
energies = energies[energies != 0]

energiesSymm = energiesSymm[compressionsSymm < 0.7]
compressionsSymm = compressionsSymm[compressionsSymm < 0.7]

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(compressions0, energies0, "ro", markersize=1)
ax.plot(compressions, energies, "bo", markersize=1)

ax.set_ylabel(r"Relative energy $\Delta E = E - E_{\text{symm}}$")
ax.set_xlabel(r"Compression $\Delta$")
ax.set_xlim(0.53, 0.66)
ax.set_ylim(-0.00015, 0.00030)
#ax.set_title(r"$N_{\text{even}} = 20$")

#ax.annotate("A*", xy=(0.56, 0.00002), xytext=(0.54, 0.00005),
#            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
#ax.annotate("B", xy=(0.565, -0.00001), xytext=(0.55, -0.00005),
#            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
#ax.annotate("C*", xy=(0.60, 0.00004), xytext=(0.615, 0.00005),
#            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
#ax.annotate("D", xy=(0.60, -0.000045), xytext=(0.61, -0.00010),
#            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
#ax.annotate("E*", xy=(0.632, 0.00005), xytext=(0.643, 0.00007),
#            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
#ax.annotate("F", xy=(0.635, -0.00010), xytext=(0.64, -0.00006),
#            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))


ax.annotate("Unstable structures", xy=(0.564, 0.00002), xytext=(0.555, 0.00020),
            arrowprops=dict(arrowstyle="-|>", color="k", relpos=(0.5, 0)))
ax.annotate("Unstable structures", xy=(0.601, 0.00004), xytext=(0.555, 0.00020),
            arrowprops=dict(arrowstyle="-|>", color="k", relpos=(0.5, 0)))
ax.annotate("Unstable structures", xy=(0.632, 0.00005), xytext=(0.555, 0.00020),
            arrowprops=dict(arrowstyle="-|>", color="k", relpos=(0.5, 0)))

ax.annotate("Stable structure", xy=(0.565, -0.000004), xytext=(0.56, -0.00012),
            arrowprops=dict(arrowstyle="-|>", color="k", relpos=(0.5, 0)))
ax.annotate("Stable structure", xy=(0.60, -0.00004), xytext=(0.56, -0.00012),
            arrowprops=dict(arrowstyle="-|>", color="k", relpos=(0.5, 0)))
ax.annotate("Stable structure", xy=(0.635, -0.00010), xytext=(0.56, -0.00012),
            arrowprops=dict(arrowstyle="-|>", color="k", relpos=(0.5, 0)))

ax.text(0.631, 0.00001, "Symmetric", color="red")

#ax.text(0.496, 0.00033, "(a)")

ax.grid()

#ax = fig.add_axes([0.36, 0.614, 0.36, 0.28])
#
#ax.plot(np.array([0, compressionsSymm[0]]), np.array([0, energiesSymm[0]]), "b--", linewidth=1.)
#ax.plot(compressionsSymm, energiesSymm, "bo", markersize=1.)
#ax.plot(compressionsSymm[0], energiesSymm[0], "ro", markersize=2.)
#print(compressionsSymm[0])
#ax.plot(compressionsSymm[np.round(compressionsSymm, 3) == 0.503], energiesSymm[np.round(compressionsSymm, 3) == 0.503], "ro", markersize=2.)
#
#ax.set_ylim(0, max(energiesSymm))
#ax.set_xlim(0, max(compressionsSymm))
#ax.set_ylabel(r"Energy $E_{\text{Symm}}$", fontsize=10)
#ax.set_xlabel("Compressions $\Delta$", fontsize=10)
#ax.tick_params(axis='both', which='major', labelsize=8)
#
#ax.text(0.05, 0.03, "S1", fontsize=10, color="red")
#ax.text(0.42, 0.12, "S2", fontsize=10, color="red")

fig.savefig("../Plots/Theory/ChaosPlots/EnergiesCompressions-{}.pdf".format(N))
