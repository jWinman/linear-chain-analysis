import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco

revolutions = 5
Ns = np.array([34, 34, 34, 34, 35, 35, 35])
Ls = np.array([1, 2, 4, 5, 2, 3, 4])
lefts = [8, 8, 8, 8, 5, 8, 8]
inflections1 = np.zeros(len(Ns))
inflections2 = np.zeros(len(Ns))

fit_func = lambda x, a, b, c: a * np.exp(-b * (x - c)**2)

for j, (N, L, left) in enumerate(zip(Ns, Ls, lefts)):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    thetas = np.array([])
    for i in range(1, revolutions + 1):
        thetas_i = np.loadtxt("../data/N{}-L{}/Thetas{}.csv".format(N, L, i))
        lbound, rbound = np.argmax(thetas_i) - left, np.argmax(thetas_i) + 8
        thetas = np.append(thetas, thetas_i[lbound:rbound])

    thetas = np.reshape(thetas, (revolutions, len(thetas) // revolutions))
    thetas_mean = np.mean(thetas, axis=0)
    thetas_std = np.std(thetas, axis=0)

    ns = np.linspace(0, len(thetas_mean), 500) + lbound
    ns_fit = np.arange(lbound, rbound)
    (a, b, c), pcov = sco.curve_fit(fit_func, ns_fit, thetas_mean, p0=[max(thetas_mean), 0.4, np.argmax(thetas_mean) + lbound])
    a_err, b_err, c_err = np.sqrt(np.diag(pcov))

    inflections1[j], inflections2[j] = np.array([-np.sqrt(1 / (2 * b)), np.sqrt(1 / (2 * b))])

    ax.plot(ns, fit_func(ns, a, b, c))
    ax.errorbar(np.arange(lbound, rbound), thetas_mean, yerr=thetas_std, fmt="o")
    ax.plot(inflections1[j] + c, fit_func(inflections1[j] + c, a, b, c), "g+", markersize=15)
    ax.plot(inflections2[j] + c, fit_func(inflections2[j] + c, a, b, c), "g+", markersize=15)

    ax.set_xlim(lbound, rbound)
    ax.set_xlabel(r"Sphere number n")
    ax.set_ylabel(r"Angles $\theta$")

    fig.savefig("../Plots/N{}-L{}/AllAngles.pdf".format(N, L))

print(inflections1, inflections2)
np.savetxt("../data/inflections.csv", np.array([inflections1, inflections2]).T)
