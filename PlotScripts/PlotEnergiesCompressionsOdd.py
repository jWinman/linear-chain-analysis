import numpy as np
import matplotlib.pyplot as plt

N = 19
compressions, energies = np.loadtxt("../data/Theory/CompressionsEnergiesDiff-{}.csv".format(N), unpack=True)
compressionsSymm, energiesSymm = np.loadtxt("../data/Theory/CompressionsEnergiesSymm-{}.csv".format(N), unpack=True)

compressions0 = compressions[energies == 0]
energies0 = energies[energies == 0]
compressions = compressions[energies != 0]
energies = energies[energies != 0]

energiesSymm = energiesSymm[compressionsSymm < 0.7]
compressionsSymm = compressionsSymm[compressionsSymm < 0.7]

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(compressions0, energies0, "ro", markersize=1)
ax.plot(compressions, energies, "bo", markersize=1)

ax.set_ylabel("Relative energy $\Delta E$")
ax.set_xlabel(r"Compression $\Delta$")
ax.set_xlim(0.53, 0.66)
ax.set_ylim(-0.00035, 0.0001)
ax.set_title(r"$N_{\text{odd}} = 19$")

#ax.annotate("A*", xy=(0.56, 0.00002), xytext=(0.54, 0.00005),
#            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
ax.annotate("B", xy=(0.555, -0.00003), xytext=(0.54, -0.00007),
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
ax.annotate("C*", xy=(0.60, -0.000035), xytext=(0.615, -0.00007),
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
ax.annotate("D", xy=(0.60, -0.00012), xytext=(0.58, -0.00016),
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
ax.annotate("E*", xy=(0.629, -0.00012), xytext=(0.64, -0.00016),
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
ax.annotate("F", xy=(0.629, -0.00025), xytext=(0.61, -0.00025),
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"))
ax.text(0.637, 0.00001, "S*", color="red")
ax.annotate("S", xy=(0.526, 0.), xytext=(0.535, 0.00004), color="r",
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="r"))

ax.text(0.496, 0.00014, "(b)")

ax.grid()

#ax = fig.add_axes([0.324, 0.66, 0.4, 0.28])
#
#ax.plot(compressions0, energies0, "ro", markersize=1)
#ax.plot(compressions, energies, "bo", markersize=1)
#
#ax.set_ylim(-1e-12, 1e-12)
#ax.set_xlim(0.525, 0.55)
#ax.set_ylabel(r"Energy $E_{\text{Symm}}$", fontsize=10)
#ax.set_xlabel("Compressions $\Delta L$", fontsize=10)
#ax.tick_params(axis='both', which='major', labelsize=8)

fig.savefig("../Plots/Theory/ChaosPlots/EnergiesCompressions-{}.pdf".format(N))
