import numpy as np
import matplotlib.pyplot as plt
import glob
import scipy.optimize as sco

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

PosEFiles = glob.glob("../data/Theory/Bifurcation/Pos*.csv")
PosEFiles = sorted(PosEFiles)

colors = ["b", "orange", "r"]
j = 0

quadratic = lambda x, a, b, c: a * (x - b)**2 + c

Energies = np.array([])
peaks = np.array([])

for i, PosEFile in enumerate(PosEFiles):
    x, y = np.loadtxt(PosEFile, unpack=True)
    centre = (x[-1] - 0.5) / 2.
    y = abs(y)

#    if (i == 0):
    (a, b, c), _ = sco.curve_fit(quadratic, x[np.argmax(y) - 1:np.argmax(y) + 2], y[np.argmax(y) - 1:np.argmax(y) + 2])
    xs_lin = np.linspace(b - 2, b + 2, 100)
    ax.axvline(b, linestyle=":", linewidth=1., color="r")
    ax.axvline(centre, linestyle="--", linewidth=1., color="k")
    ax.plot(x, y, "o", markersize=3.)
    j += 1

    Energies = np.append(Energies, 0.5 * sum(y**2))
    peaks = np.append(peaks, abs(b - centre))

Energies = Energies[np.argsort(peaks)]
Energies = Energies - Energies[0]
peaks = np.sort(peaks)
np.savetxt("../data/Theory/Bifurcation/PeaksEnergies.csv", np.array([Energies - Energies[0], peaks]).T)

ax.set_ylabel("Displacement $r$")
ax.set_xlabel("Positions $x$")

fig.savefig("../Plots/Theory/HigherEnergies/ExampleProfile.pdf")

figEnergies = plt.figure()
ax = figEnergies.add_subplot(1, 1, 1)

ax.plot(peaks, Energies, "o")

figEnergies.savefig("../Plots/Theory/HigherEnergies/PeakEnergies.pdf")
