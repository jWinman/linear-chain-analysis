import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco

N = 20
epss, forces0, compressions, As, Bs = np.loadtxt("../data/Theory/FitParams-{}.csv".format(N), unpack=True)
epss, forces0, compressions = epss[Bs != 0], forces0[Bs != 0], compressions[Bs != 0]
As, Bs = As[Bs != 0], Bs[Bs != 0]

B_theo = lambda eps: np.sqrt(eps)
A_theo = lambda F0, eps: 4 * F0 / np.sqrt(eps)

epss_theo = np.linspace(0, 1.0, 1000)

fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)

ax.plot(epss[::10], As[::10], ".", label="Fit values")
ax.plot(epss, A_theo(forces0, epss), "-", label="theoretical values")
ax.set_ylabel("$A$")
ax.set_xlabel("$\epsilon$")
ax.legend()

ax = fig.add_subplot(2, 1, 2)

ax.plot(epss[::10], Bs[::10], ".", label="Fit values")
ax.plot(epss_theo, B_theo(epss_theo), "-", label="theoretical values")
ax.set_ylabel("$B$")
ax.set_xlabel("$\epsilon$")
ax.set_xlim(left=0)
ax.set_ylim(bottom=0)
ax.legend()

fig.savefig("../Plots/Theory/FitParams-{}.pdf".format(N))
