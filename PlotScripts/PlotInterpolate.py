import numpy as np
import matplotlib.pyplot as plt

Energies, peaks = np.loadtxt("../data/Theory/Bifurcation/PeaksEnergies.csv", unpack=True)
peaks = peaks[:5]
Energies = Energies[:5]

matrix = np.zeros((2 * len(peaks), 2 * len(peaks)))

for i in range(len(matrix)):
    for j in range(len(matrix.T)):
        if (i < len(peaks)):
            matrix[i, j] = peaks[i]**j
        else:
            reset = len(peaks)
            matrix[i, j] = j * peaks[i - reset]**(j - 1)

b = np.zeros(2 * len(peaks))
b[:len(peaks)] = Energies

As = np.linalg.solve(matrix, b)
print(As, len(As))

def fit_func(x, As):
    summe = 0
    for i, a in enumerate(As):
        summe += x**i * a
    return summe

x = np.linspace(0, 2, 1000)


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(peaks, Energies, "o")
ax.plot(x, fit_func(x, As), "-")

ax.set_xlabel("Peak position")
ax.set_ylabel("Relative energies $\Delta E$")
ax.set_ylim(-0.0001, 0.00033)

ax.text(0, -0.000035, "S", ha="center")
ax.text(0.5, 0.00028, "A*", ha="center")
ax.text(1.0, -0.00004, "B", ha="center")
ax.text(1.5, 0.000265, "C*", ha="center")
ax.text(2.0, -0.00006, "D", ha="center")

ax.annotate("Interpolated \n Peierls--Nabarro \n potential",
            xy=(0.65, 0.0002), xytext=(0.70, 0.00025),
            arrowprops=dict(facecolor='black', arrowstyle="->", relpos=(0., 0)))
ax.annotate("Interpolated \n Peierls--Nabarro \n potential",
            xy=(1.35, 0.000186), xytext=(0.70, 0.00025),
            arrowprops=dict(facecolor='black', arrowstyle="->", relpos=(0.8, 0)))

fig.savefig("../Plots/Theory/HigherEnergies/PeaksEnergies.pdf")
